from django.urls import path

from . import views

urlpatterns = [
    path('done/', views.allTodo, name='finished'),
    path('notdone/', views.notDone, name='notfinished'),
    path('all/', views.allTodo, name='all'),
]