from django.db import models

class todo(models.Model):
    description = models.CharField(max_length=200)
    todo_date = models.DateTimeField('todo date')
    finished = models.BooleanField(False)

    def __str__(self):
        return self.description
    
    def is_finished(self):
        return self.finished


