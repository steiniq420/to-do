from http.client import HTTPResponse
from django.shortcuts import render

def detail(request, todo_id):
    return HTTPResponse("You're looking at to-do %s" % todo_id)

# Return a view of all completed todos
def done(request):
    return HTTPResponse("You're looking at finished todos")

# Return a view of all not completed todos
def notDone(request):
    return HTTPResponse("You'are looking at unfinished todos")

# Return all the todo 
def allTodo(request):
    return HTTPResponse("You're looking at all todos")